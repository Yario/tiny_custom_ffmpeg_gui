![](GUI_example.png)  
  
I used to run custom ffmpeg commands to compress and cut my drone videos, utilizing my GPU via cuda.  
This is a simple QT5 based GUI for the command that I used.  
(generated with the help of GPT 3.5; aint nobody time for simple but time consuming stuff ;-) )  
  
ffmpeg -hwaccel cuda -hwaccel_output_format cuda -ss 00:01:20 -to 00:02:20 -i 1.MP4 -c:a copy -c:v hevc_nvenc -b:v 10M 1b.mp4
