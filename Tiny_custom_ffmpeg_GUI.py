import sys
from PyQt5.QtCore import QFile
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QLineEdit, QPushButton, QFileDialog, QMessageBox
import subprocess

class FFmpegGUI(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("FFmpeg GUI")
        self.setGeometry(200, 200, 700, 300)

        self.input_label = QLabel("Input File:", self)
        self.input_label.move(20, 20)

        self.input_entry = QLineEdit(self)
        self.input_entry.setGeometry(150, 20, 400, 20)

        self.input_button = QPushButton("Browse", self)
        self.input_button.setGeometry(540, 20, 80, 20)
        self.input_button.clicked.connect(self.select_input_file)

        self.output_label = QLabel("Output File:", self)
        self.output_label.move(20, 60)

        self.output_entry = QLineEdit(self)
        self.output_entry.setGeometry(150, 60, 400, 20)

        self.output_button = QPushButton("Browse", self)
        self.output_button.setGeometry(540, 60, 80, 20)
        self.output_button.clicked.connect(self.select_output_file)

        self.start_label = QLabel("Start Time", self)
        self.start_label.move(20, 100)
        #self.start_label.setStyleSheet("font-weight:bold")

        self.start_label_sub = QLabel("(HH:MM:SS):", self)
        self.start_label_sub.move(20, 120)

        self.start_entry = QLineEdit(self)
        self.start_entry.setGeometry(150, 110, 100, 20)

        self.end_label = QLabel("End Time", self)
        self.end_label.move(20, 150)
        #self.end_label.setStyleSheet("font-weight:bold")

        self.end_label_sub = QLabel("(HH:MM:SS):", self)
        self.end_label_sub.move(20, 170)

        self.end_entry = QLineEdit(self)
        self.end_entry.setGeometry(150, 160, 100, 20)

        self.bitrate_label = QLabel("Output Bitrate (e.g., 10M):", self)
        self.bitrate_label.move(20, 200)
        #self.bitrate_label.setStyleSheet("font-weight:bold")

        self.bitrate_entry = QLineEdit(self)
        self.bitrate_entry.setGeometry(150, 210, 100, 20)

        self.run_button = QPushButton("Run FFmpeg", self)
        self.run_button.setGeometry(180, 240, 140, 30)
        self.run_button.clicked.connect(self.run_ffmpeg)

    def select_input_file(self):
        file_path, _ = QFileDialog.getOpenFileName(self, "Select Input File", "", "Video files (*.mp4)")
        self.input_entry.setText(file_path)

    def select_output_file(self):
        file_path, _ = QFileDialog.getSaveFileName(self, "Select Output File", "", "Video files (*.mp4)")
        self.output_entry.setText(file_path)

    def run_ffmpeg(self):
        input_file = self.input_entry.text()
        output_file = self.output_entry.text()
        start_time = self.start_entry.text()
        end_time = self.end_entry.text()
        bitrate = self.bitrate_entry.text()

        # Check if the output file already exists
        if output_file and QFile.exists(output_file):
            reply = QMessageBox.warning(self, "File Already Exists",
                                        f"The output file '{output_file}' already exists. "
                                        f"Overwriting the file will delete its contents. "
                                        "Do you want to proceed?",
                                        QMessageBox.Yes | QMessageBox.No)
            if reply == QMessageBox.No:
                return

        ffmpeg_cmd = f'ffmpeg -hwaccel cuda -hwaccel_output_format cuda -ss {start_time} -to {end_time} ' \
                     f'-i "{input_file}" -c:a copy -c:v hevc_nvenc -b:v {bitrate} "{output_file}"'
        # Run the ffmpeg command using subprocess or any other method you prefer
        #print(ffmpeg_cmd)
        subprocess.run(ffmpeg_cmd, shell=True)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ffmpeg_gui = FFmpegGUI()
    ffmpeg_gui.show()
    sys.exit(app.exec_())
